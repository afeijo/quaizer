<article<?php print $attributes; ?>>
  <?php print $user_picture; ?>
  
  <?php if (!$page && $title): ?>
  <header>
    <?php print render($title_prefix); ?>
    <h2<?php print $title_attributes; ?>><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h2>
    <?php print render($title_suffix); ?>
  </header>
  <?php endif; ?>
  
  <?php if ($display_submitted): ?>
  <footer class="submitted"><?php print $date; ?> -- <?php print $name; ?></footer>
  <?php endif; ?>  
  
  <div<?php print $content_attributes; ?>>
    <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);
      print render($content);
    ?>
  </div>
  
  <div class="clearfix">
    <?php if (!empty($content['links'])): ?>
      <nav class="links node-links clearfix"><?php print render($content['links']); ?></nav>
    <?php endif; ?>

    <br />
    <div id='ticket_cmds'>
    <?php $attr['attributes']['class'][] = 'ctools-use-modal'; //'fancybox'; ?>
    <nav class="links node-links clearfix">

<!-- <fieldset>
    <legend>Ticket functions</legend>
    <ul class="link inline">
    <li><?php $attr['attributes']['title'] = 'Send the ticket to someone else'; 
      print l(t('Escalation'), 'ticket/escalation/'.$node->nid, $attr); ?> 
    <li><?php $attr['attributes']['title'] = 'End the ticket'; 
      print l(t('Closure'), 'ticket/closure/'.$node->nid, $attr); ?>
    <?php 
    // get the status field
    $statusf = field_get_items('node', $node, 'field_status');
    // get the status current value
    $status = field_view_value('node', $node, 'field_status', $statusf[0]);
    // get the status situation (open/closed)
    $situationf = field_get_items('taxonomy_term', $status['#options']['entity'], 'field_situation');
    #kpr($situationf);
#    $situation = field_view_value('taxonomy_term', $situationf, 'field_situation', $situationf[0]);
    #kpr($situation);    
    ?>
    <?php if ($situationf[0]['value']==1): ?>
    <?php $attr['attributes']['title'] = 'Opens the ticket again';     
      print l(t('Reopening'), 'ticket/reopening/'.$node->nid, $attr); ?><br />
    <?php endif; ?>
    </ul>
</fieldset>
 -->
     </nav>
    </div>
    
    <?php print render($content['comments']); ?>
  </div>
</article>