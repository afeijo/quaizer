<?php $tag = $block->subject ? 'section' : 'div'; ?>
<!--div class='shadow-container'><div class='shadow1'><div class='shadow2'><div class='shadow3' !-->
<<?php print $tag . $attributes; ?>>
  <div class="block-inner clearfix">
    <?php print render($title_prefix); ?>
    <?php if ($block->subject): ?>
      <h2<?php print $title_attributes; ?>><?php print $block->subject; ?></h2>
    <?php endif; ?>
    <?php print render($title_suffix); ?>
    
    <div<?php print $content_attributes; ?>>
      <?php print $content ?>
    </div>
  </div>
</<?php print $tag; ?>>
<!--/div></div></div></div !-->

