<?php

define('HOUR', 1/24);

function calc_sla($nid, $p1, $p2) {
  $node = node_load($nid);
  #kpr($node);

  #$prior = field_get_items('node', $node, 'field_priority');
  #kpr($prior);

  $p = field_view_field('node', $node, 'field_priority');
  #kpr($p);

  kpr('Priority: ' . $p[0]['#title']);

  $prior = round($p1?$p1:mt_rand(1,5)*24, 3);
  $tempo = round($p2?$p2:mt_rand(1,12), 3);
  $sla = $tempo/$prior;

  $output = "tempo ticket/prior=sla<br />";
  $output .= "{$tempo}h/{$prior}h=$sla";
  return $output;
}

function step() {
	static $step;
	foreach(func_get_args() as $a)
		out('step ' . ++$step, $a);
}

function pout() {
	foreach(func_get_args() as $a)
		drupal_set_message(
			str_replace(array('{','}'), '', print_r($a,1) ),
		'debug');
}
function out() {
	global $user;
	if ($user->uid && $user->uid<5) {
	  if (func_num_args()>1 && is_array(func_get_arg(1))) {
	    $func = func_get_arg(0);
	    $params = func_get_arg(1);
	    pout(call_user_func_array('sprintf', array_merge(array($func), $params)));
	  } else
  		foreach(func_get_args() as $a)
  			pout($a);
	}
}
function outt($msg=null, $out=0) {
	static $t;
	if (!$t) $t=microtime(true);
	if ((oc('home', 'devx') || $out) && $msg)
		if ($out)
			out($msg .': ' .number_format(microtime(true)-$t, 3) ."\n"); else
			echo($msg .': ' .number_format(microtime(true)-$t, 3) ."\n");
}

function out2() {
	global $user;
	if ($user->uid && $user->uid<4)
	foreach(func_get_args() as $a)
		drupal_set_message(
			nl2br(str_replace(array(' ', '{','}'), array('&nbsp;', '&nbsp;', ''), print_r($a, true))),
		'debug');
}
function out3() {
	global $user;
	if ($user->uid && $user->uid<4)
	foreach(func_get_args() as $a)
		echo '<textarea rows=8 cols=60>', $a, '</textarea>';
}

/**
 * A function that returns the taxonomy tree as an array to be output in an select object
 */
function taxonomy_get_tree_array($vid) {
  $output = array();
  $voc = taxonomy_get_tree($vid);
  foreach($voc as $term) {
    $output[$term->tid] = $term->name;
  }
  return $output;
}