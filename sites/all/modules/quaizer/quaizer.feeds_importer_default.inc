<?php
/**
 * @file
 * quaizer.feeds_importer_default.inc
 */

/**
 * Implements hook_feeds_importer_default().
 */
function quaizer_feeds_importer_default() {
  $export = array();

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'inbox';
  $feeds_importer->config = array(
    'name' => 'Inbox',
    'description' => 'Open tickets from email (the mailhandler module)',
    'fetcher' => array(
      'plugin_key' => 'MailhandlerFetcher',
      'config' => array(
        'filter' => 'MailhandlerFilters',
      ),
    ),
    'parser' => array(
      'plugin_key' => 'MailhandlerParser',
      'config' => array(
        'available_commands' => 'status',
        'authenticate_plugin' => 'MailhandlerAuthenticateDefault',
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'expire' => '-1',
        'author' => '1',
        'authorize' => 0,
        'mappings' => array(
          0 => array(
            'source' => 'subject',
            'target' => 'title',
            'unique' => FALSE,
          ),
          1 => array(
            'source' => 'authenticated_uid',
            'target' => 'uid',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'status',
            'target' => 'status',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'body_html',
            'target' => 'body',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => 'message_id',
            'target' => 'guid',
            'unique' => 1,
          ),
          5 => array(
            'source' => 'authenticated_uid',
            'target' => 'field_user_id_inbox',
            'unique' => FALSE,
          ),
          6 => array(
            'source' => 'sender-address',
            'target' => 'field_sender_address',
            'unique' => FALSE,
          ),
          7 => array(
            'source' => 'to-address',
            'target' => 'field_to_address',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '0',
        'input_format' => 'filtered_html',
        'skip_hash_check' => 0,
        'bundle' => 'inbox',
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => '3600',
    'expire_period' => 3600,
    'import_on_create' => 1,
    'process_in_background' => 0,
  );
  $export['inbox'] = $feeds_importer;

  return $export;
}
