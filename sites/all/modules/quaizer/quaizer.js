(function($) {

  $(document).ready(function(){

  $('select[name^=field_urgency], select[name^=field_impact]')
   .bind('change', function(){
    urg = parseInt(Drupal.settings.urgency[$('select[name^=field_urgency]').val()]);
    imp = parseInt(Drupal.settings.impact[$('select[name^=field_impact]').val()]);
    tot = (urg + imp) - 1;
//    console.log(urg + ' ' + imp + ' = ' + tot);
    $('select[name^=field_priority]').val(Drupal.settings.priority[tot]);
  });
  
  $obj = $('#edit-field-user-und-0-uid-autocomplete');
  var path = $obj.val();
    
    
if (0)
  $('.fancybox').fancybox({'width':600,'height':370,'padding':7,'xtype':'iframe'});
  // ajaxForm para interceptar o submit no iframe do fancybox, e recarregar toda página
  $('form').live('ajaxForm', function() {
    parent.location.reload(true);
  });

  // passa o nome da empresa pro autocomplete do usuário (removendo o [nid:n])
/*
  $('#edit-field-company-und-0-nid').bind('blur', function() {
    $obj.val(path + '/' + this.value.replace(/\s*\[nid:\d+\]\s*REMOVER/i, ''));
    console.log($obj.val());
  });
*/

  // autocomplete do campo Empresa
  $('#edit-field-company-und-0-nid').bind('blur', function() {
    $.get(Drupal.settings.basePath + 'quaizer/autocomplete/company/user/' + this.value.replace(/\s*\[nid:\d+\]\s*/i, ''), function(data) {
      opt = '';
      for (rec in data) {
        opt += '<option>' + rec;
      }
      $('#edit-custom-user').attr({size: 5, disabled: 0}).html(opt);
    });
  });

  $('#edit-custom-user').bind('change blur', function() {
    if (this.value>'0') {
      $('div#ticket_fields').fadeIn();
    } else {
      $('div#ticket_fields').fadeOut();
    }
  });
  
  if ($('#edit-field-company-und-0-nid').val())
    $('#ticket_fields').show();
  if (!$('#edit-title').val() && $('div.messages').size()==0)
    $('#ticket_fields').hide();

})

})(jQuery);
