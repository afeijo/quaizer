<?php
/**
 * @file
 * quaizer.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function quaizer_default_rules_configuration() {
  $items = array();
  $items['rules_create_ticket_from_inbox'] = entity_import('rules_config', '{ "rules_create_ticket_from_inbox" : {
      "LABEL" : "Create Ticket from Inbox",
      "PLUGIN" : "reaction rule",
      "REQUIRES" : [ "rules" ],
      "ON" : [ "node_insert" ],
      "IF" : [
        { "node_is_of_type" : { "node" : [ "node" ], "type" : { "value" : { "inbox" : "inbox" } } } }
      ],
      "DO" : [
        { "entity_create" : {
            "USING" : {
              "type" : "node",
              "param_type" : "ticket",
              "param_title" : [ "node:title" ],
              "param_author" : "1"
            },
            "PROVIDE" : { "entity_created" : { "entity_created" : "Created entity" } }
          }
        },
        { "data_set" : { "data" : [ "entity-created:body" ], "value" : [ "node:body" ] } },
        { "data_set" : { "data" : [ "entity-created:field-method" ], "value" : "26" } },
        { "data_set" : {
            "data" : [ "entity-created:field-group" ],
            "value" : { "value" : { "1" : "1" } }
          }
        },
        { "data_set" : {
            "data" : [ "entity-created:field-user:mail" ],
            "value" : [ "node:field-user-id-inbox" ]
          }
        },
        { "data_set" : { "data" : [ "entity-created:field-user" ], "value" : [ "node:author" ] } },
        { "data_set" : {
            "data" : [ "entity-created:field-user:field-company" ],
            "value" : [ "node:source:author:field-company" ]
          }
        },
        { "entity_save" : { "data" : [ "entity-created" ], "immediate" : 1 } }
      ]
    }
  }');
  $items['rules_im_automatic_redirect_after_login'] = entity_import('rules_config', '{ "rules_im_automatic_redirect_after_login" : {
      "LABEL" : "IM Automatic Redirect After Login",
      "PLUGIN" : "reaction rule",
      "REQUIRES" : [ "rules" ],
      "ON" : [ "user_login" ],
      "DO" : [ { "redirect" : { "url" : "catalog" } } ]
    }
  }');
  $items['rules_im_unique_incident_created_for_high_priority'] = entity_import('rules_config', '{ "rules_im_unique_incident_created_for_high_priority" : {
      "LABEL" : "IM UNIQUE - Incident created for high priority",
      "PLUGIN" : "reaction rule",
      "REQUIRES" : [ "rules" ],
      "ON" : [ "node_insert" ],
      "IF" : [
        { "node_is_of_type" : { "node" : [ "node" ], "type" : { "value" : { "ticket" : "ticket" } } } },
        { "data_is" : { "data" : [ "node:field-priority" ], "value" : "7" } }
      ],
      "DO" : [
        { "mail_to_users_of_role" : {
            "roles" : { "value" : { "5" : "5" } },
            "subject" : "A new ticket - [node:nid] - was created with HIGH priority",
            "message" : "Open your eyes, something important is happening.\\r\\n\\r\\nGive a special attention.\\r\\n\\r\\nThe incident number is [node:nid]. \\r\\n\\r\\nSee more visiting [node:url]\\r\\n\\r\\nRegards,\\r\\n\\r\\nSupport Center",
            "from" : "[site:mail]"
          }
        }
      ]
    }
  }');
  $items['rules_im_unique_incident_created_for_specific_categorization'] = entity_import('rules_config', '{ "rules_im_unique_incident_created_for_specific_categorization" : {
      "LABEL" : "IM UNIQUE - Incident created for specific categorization",
      "PLUGIN" : "reaction rule",
      "REQUIRES" : [ "rules" ],
      "ON" : [ "node_insert" ],
      "IF" : [
        { "node_is_of_type" : { "node" : [ "node" ], "type" : { "value" : { "ticket" : "ticket" } } } },
        { "data_is" : { "data" : [ "node:field-category:0" ], "value" : "36" } }
      ],
      "DO" : [
        { "mail_to_users_of_role" : {
            "roles" : { "value" : { "5" : "5" } },
            "subject" : "A new ticket - [node:nid] - was created for specific categorization",
            "message" : "Hi, boys!\\r\\n\\r\\nStay alert, because an incident has been opened for the following categorization:\\r\\n\\r\\n(...) [node:field_category] (...)\\r\\n\\r\\nThe incident number is [node:nid]. \\r\\n\\r\\nSee more visiting [node:url]\\r\\n\\r\\nRegards,\\r\\n\\r\\nSupport Center",
            "from" : "[site:mail]"
          }
        }
      ]
    }
  }');
  $items['rules_im_unique_incident_created_for_specific_user'] = entity_import('rules_config', '{ "rules_im_unique_incident_created_for_specific_user" : {
      "LABEL" : "IM UNIQUE - Incident created for specific user",
      "PLUGIN" : "reaction rule",
      "REQUIRES" : [ "rules" ],
      "ON" : [ "node_insert" ],
      "IF" : [
        { "node_is_of_type" : { "node" : [ "node" ], "type" : { "value" : { "ticket" : "ticket" } } } },
        { "data_is" : { "data" : [ "node:field-user:name" ], "value" : "Fil\\u00e9 com fritas" } }
      ],
      "DO" : [
        { "mail_to_users_of_role" : {
            "roles" : { "value" : { "5" : "5" } },
            "subject" : "A new ticket - [node:nid] - was created to [node:field_user]",
            "message" : "It\\u0027s hard to say, but this person is very important.\\r\\n\\r\\nGive a special attention.\\r\\n\\r\\nThe incident number is [node:nid]. \\r\\n\\r\\nSee more visiting [node:url]\\r\\n\\r\\nRegards,\\r\\n\\r\\nSupport Center",
            "from" : "[site:mail]"
          }
        }
      ]
    }
  }');
  $items['rules_im_unique_incident_scheduled_for_automatic_close'] = entity_import('rules_config', '{ "rules_im_unique_incident_scheduled_for_automatic_close" : {
      "LABEL" : "IM UNIQUE - Incident scheduled for automatic close",
      "PLUGIN" : "reaction rule",
      "REQUIRES" : [ "rules", "rules_scheduler" ],
      "ON" : [ "node_update" ],
      "IF" : [
        { "node_is_of_type" : { "node" : [ "node" ], "type" : { "value" : { "ticket" : "ticket" } } } },
        { "data_is" : { "data" : [ "node:field-status" ], "value" : "37" } }
      ],
      "DO" : [
        { "schedule" : {
            "component" : "rules_to_close_incident",
            "date" : "+3 minutes",
            "identifier" : "To close incident [node:nid]",
            "param_node" : [ "node" ]
          }
        }
      ]
    }
  }');
  $items['rules_im_unique_incident_solved_awaiting_confirmation'] = entity_import('rules_config', '{ "rules_im_unique_incident_solved_awaiting_confirmation" : {
      "LABEL" : "IM UNIQUE - Incident solved - awaiting confirmation",
      "PLUGIN" : "reaction rule",
      "REQUIRES" : [ "rules" ],
      "ON" : [ "node_update" ],
      "IF" : [
        { "node_is_of_type" : { "node" : [ "node" ], "type" : { "value" : { "ticket" : "ticket" } } } },
        { "data_is" : { "data" : [ "node:field-status" ], "value" : "37" } }
      ],
      "DO" : []
    }
  }');
  $items['rules_im_unique_schedule_for_automatic_close'] = entity_import('rules_config', '{ "rules_im_unique_schedule_for_automatic_close" : {
      "LABEL" : "IM UNIQUE - schedule for automatic close",
      "PLUGIN" : "reaction rule",
      "ON" : [],
      "DO" : []
    }
  }');
  $items['rules_rm_request_url_access'] = entity_import('rules_config', '{ "rules_rm_request_url_access" : {
      "LABEL" : "RM Request URL Access",
      "PLUGIN" : "reaction rule",
      "REQUIRES" : [ "webform_rules", "rules" ],
      "ON" : [ "webform_rules_submit" ],
      "IF" : [
        { "webform_has_id" : {
            "form_id" : [ "form_id" ],
            "selected_webform" : { "value" : { "webform-client-form-55" : "webform-client-form-55" } }
          }
        }
      ],
      "DO" : [
        { "entity_create" : {
            "USING" : {
              "type" : "node",
              "param_type" : "ticket",
              "param_title" : "Request to URL Access",
              "param_author" : [ "user" ]
            },
            "PROVIDE" : { "entity_created" : { "ticket_created" : "Created ticket" } }
          }
        },
        { "data_set" : {
            "data" : [ "ticket-created:body:value" ],
            "value" : "[data:url_desired-value]"
          }
        },
        { "data_set" : { "data" : [ "ticket-created:field-ticket-type" ], "value" : "1" } },
        { "data_set" : {
            "data" : [ "ticket-created:field-company" ],
            "value" : [ "user:field-company" ]
          }
        },
        { "data_set" : { "data" : [ "ticket-created:field-user" ], "value" : [ "user" ] } },
        { "data_set" : { "data" : [ "ticket-created:field-category:0" ], "value" : "40" } },
        { "data_set" : { "data" : [ "ticket-created:field-category:1" ], "value" : "41" } },
        { "data_set" : { "data" : [ "ticket-created:field-category:2" ], "value" : "42" } },
        { "data_set" : {
            "data" : [ "ticket-created:field-group" ],
            "value" : { "value" : { "43" : "43" } }
          }
        },
        { "entity_save" : { "data" : [ "ticket-created" ], "immediate" : 1 } },
        { "drupal_message" : { "message" : "Seu formul\\u00e1rio foi submetido!" } }
      ]
    }
  }');
  $items['rules_rule_component_ticket_accounts'] = entity_import('rules_config', '{ "rules_rule_component_ticket_accounts" : {
      "LABEL" : "Rule component ticket accounts",
      "PLUGIN" : "action set",
      "REQUIRES" : [ "rules" ],
      "ACCESS_EXPOSED" : "1",
      "USES VARIABLES" : { "user" : { "label" : "user", "type" : "user" } },
      "ACTION SET" : [
        { "redirect" : { "url" : "catalog", "destination" : 1 } },
        { "drupal_message" : { "message" : "Parab\\u00e9ns, sua solicita\\u00e7\\u00e3o foi enviada com sucesso!!" } },
        { "entity_create" : {
            "USING" : {
              "type" : "node",
              "param_type" : "ticket",
              "param_title" : "Criar uma nova conta skype",
              "param_author" : [ "site:current-user" ]
            },
            "PROVIDE" : { "entity_created" : { "entity_created" : "Created entity" } }
          }
        },
        { "data_set" : { "data" : [ "entity-created:field-ticket-type" ], "value" : "1" } },
        { "data_set" : {
            "data" : [ "entity-created:field-category" ],
            "value" : { "value" : { "86" : "86" } }
          }
        },
        { "data_set" : {
            "data" : [ "entity-created:field-company" ],
            "value" : [ "user:field-company" ]
          }
        },
        { "data_set" : { "data" : [ "entity-created:field-user" ], "value" : [ "user" ] } },
        { "data_set" : {
            "data" : [ "entity-created:field-group" ],
            "value" : { "value" : { "2" : "2" } }
          }
        },
        { "data_set" : { "data" : [ "entity-created:field-method" ], "value" : "87" } },
        { "data_set" : { "data" : [ "entity-created:field-priority" ], "value" : "6" } },
        { "data_set" : { "data" : [ "entity-created:field-urgency" ], "value" : "18" } },
        { "data_set" : { "data" : [ "entity-created:field-impact" ], "value" : "21" } },
        { "data_set" : { "data" : [ "entity-created:field-reason" ], "value" : "10" } },
        { "data_set" : {
            "data" : [ "entity-created:body:value" ],
            "value" : "Usu\\u00e1rio: [user:name] \\r\\nE-mail: [user:mail] "
          }
        },
        { "entity_save" : { "data" : [ "entity-created" ], "immediate" : 1 } }
      ]
    }
  }');
  $items['rules_to_close_incident'] = entity_import('rules_config', '{ "rules_to_close_incident" : {
      "LABEL" : "To close incident",
      "PLUGIN" : "rule",
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : { "node" : { "label" : "Node", "type" : "node" } },
      "IF" : [
        { "node_is_of_type" : { "node" : [ "node" ], "type" : { "value" : { "ticket" : "ticket" } } } },
        { "data_is" : { "data" : [ "node:field-status" ], "value" : "37" } }
      ],
      "DO" : [
        { "data_set" : { "data" : [ "node:field-status" ], "value" : "31" } },
        { "mail_to_users_of_role" : {
            "roles" : { "value" : { "5" : "5" } },
            "subject" : "This incident - [node:nid] was closed by the system",
            "message" : "The user responsible for the incident did not closed in the agreed period.\\r\\n\\r\\nThe incident was closed by the system.\\r\\n\\r\\nSupport Center",
            "from" : "[site:mail]"
          }
        }
      ]
    }
  }');
  return $items;
}
