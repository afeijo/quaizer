<?php
/**
 * @file
 * quaizer.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function quaizer_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "feeds" && $api == "feeds_importer_default") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function quaizer_views_api() {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function quaizer_node_info() {
  $items = array(
    'company' => array(
      'name' => t('Company'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Name'),
      'help' => '',
    ),
    'inbox' => array(
      'name' => t('Inbox'),
      'base' => 'node_content',
      'description' => t('Estrutura utilizada para receber os emails recebidos pelo módulo MailHandler e servir de base para as regras que criação os tickets'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'knowledge_base' => array(
      'name' => t('Knowledge Base'),
      'base' => 'node_content',
      'description' => t('Store known error database'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'ticket' => array(
      'name' => t('Ticket'),
      'base' => 'node_content',
      'description' => t('Cadastre os atendimentos recebidos'),
      'has_title' => '1',
      'title_label' => t('Título'),
      'help' => '',
    ),
  );
  return $items;
}
