<?php
/**
 * @file
 * quaizer.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function quaizer_user_default_roles() {
  $roles = array();

  // Exported role: analyst.
  $roles['analyst'] = array(
    'name' => 'analyst',
    'weight' => 3,
  );

  // Exported role: end user.
  $roles['end user'] = array(
    'name' => 'end user',
    'weight' => 6,
  );

  // Exported role: kba.
  $roles['kba'] = array(
    'name' => 'kba',
    'weight' => 7,
  );

  // Exported role: supervisor.
  $roles['supervisor'] = array(
    'name' => 'supervisor',
    'weight' => 4,
  );

  return $roles;
}
