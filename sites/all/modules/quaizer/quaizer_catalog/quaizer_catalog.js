(function($) {
  // $(function(){
  Drupal.behaviors.quaizer_catalog = {
    attach: function (context, settings) {

      $('.level_1 a.term:not(.processed)', context).unbind('click').click(function(e) {
        e.preventDefault();
        $this = $(this);
        $parent = $this.parent();
        // console.log($parent.siblings());
        $parent.siblings('div').fadeOut('slow');
console.log(this);
        $parent.find('.inner-wrapper').html('carregando...');
        $./*wait(500).*/post(Drupal.settings.basePath+'catalog', {tid: $this.attr('tid'), lvl: $this.attr('lvl')}, function(data) {
          if (data) {
            $parent.find('.inner-wrapper').html(data).hide().fadeIn();
            Drupal.attachBehaviors();
          }
        })                    
      }).addClass('processed');
      
      $('a#reset', context).click(function(e) {
        e.preventDefault();
        $('.inner-wrapper').html('').hide();
        $('.level_1').fadeIn();
      });
      
    } // end of attach
  }; // end of behaviors

  // }) // Fim do $(function)

$.fn.wait = function(time, type) {
    time = time || 1000;
    type = type || "fx";
    return this.queue(type, function() {
        var self = this;
        setTimeout(function() {
            $(self).dequeue();
        }, time);
    });
};

})(jQuery);

