<?php
/**
 * @file
 * quaizer.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function quaizer_user_default_permissions() {
  $permissions = array();

  // Exported permission: quaizer admin.
  $permissions['quaizer admin'] = array(
    'name' => 'quaizer admin',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'quaizer',
  );

  return $permissions;
}
